<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="edu.ifsp.lojinha.modelo.Cliente" %>
<%@ page import="java.util.List" %>
    
<%
List<Cliente> lista = (List<Cliente>)request.getAttribute("lista");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lista de Clientes</title>

<style type="text/css">
tr.par {
	background: yellow;
}

tr.impar {
	background: blue;
}

</style>

</head>
<body>
<h1>Lista de Clientes</h1>


<table border="1">
<tr><th>Nome</th><th>Email</th></tr>
<% 
	for (int i=0; i < lista.size(); i++) {
		Cliente c = lista.get(i);
		String classe = i % 2 == 0 ? "par" : "impar"; 
%>
<tr class="<%= classe %>"><td><%= c.getNome() %></td><td><%= c.getEmail() %></td></tr>
<% } %>
</table>

</body>
</html>